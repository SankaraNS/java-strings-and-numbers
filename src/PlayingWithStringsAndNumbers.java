
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Sanka
 */
public class PlayingWithStringsAndNumbers 
{

    //the array which is going to store names and grades
    private static String[][] Chart = new String[5][6];

    //defining the methods first
    /*
    *This method initializes the array Chart
     */
    public static void initial(String[][] Chart) 
    {   //initialising 5 students along with their grades
        Chart[0][0] = "Ruut Bear";
        Chart[0][1] = "52";
        Chart[0][2] = "06";
        Chart[0][3] = "53";
        Chart[0][4] = "77";
        Chart[0][5] = "02";

        Chart[1][0] = "Al Mond";
        Chart[1][1] = "68";
        Chart[1][2] = "72";
        Chart[1][3] = "88";
        Chart[1][4] = "55";
        Chart[1][5] = "98";

        Chart[2][0] = "Chris Maas";
        Chart[2][1] = "66";
        Chart[2][2] = "77";
        Chart[2][3] = "88";
        Chart[2][4] = "99";
        Chart[2][5] = "55";

        Chart[3][0] = "Oki Doki";
        Chart[3][1] = "23";
        Chart[3][2] = "45";
        Chart[3][3] = "33";
        Chart[3][4] = "45";
        Chart[3][5] = "91";

        Chart[4][0] = "Jake Peralta";
        Chart[4][1] = "83";
        Chart[4][2] = "73";
        Chart[4][3] = "73";
        Chart[4][4] = "95";
        Chart[4][5] = "63";

    }

    /**
     * This method displays the names and marks of the students
     */
    public static void displayGrade() 
    {
        //for creating columns
        System.out.printf(" %-20s %-20s %-20s %-20s %-20s %-20s \n", "Student", "Course 1", "Course 2", "Course 3", "Course 4", "Course 5");

        //nested for loops for diplaying the contents of Chart
        for (int i = 0; i < Chart.length; i++) {

            for (int j = 0; j < Chart[i].length; j++) {
                System.out.printf(" %-20s", Chart[i][j]);

            }
            System.out.println();
        }

    }

    /**
     * Calculates the average of all the students
     */

    public static void average() 
    {

        float[] avg = new float[5];
        float sum;
        int counter;

        for (int i = 0; i < Chart.length; i++) {

            sum = 0;
            counter = 0;
            for (int j = 1; j < Chart[i].length; j++) {
                //adds up the grades
                sum += Integer.parseInt(Chart[i][j]);
                counter++;
            }
            //calculates the average of every student by dividing sum/counter and storing as an array
            avg[i] = sum / counter; //storing the average of each student in avg array
        }
        //calling the below methods to calculate the highest and the lowest average
        averageHigh(avg);
        averageLow(avg);

    }

    /**
     * This method calculates and presents the highest average of the 5 students
     *
     * @param avg
     */
    public static void averageHigh(float[] avg) 
    {

        int greatestAvg = 0;
        for (int i = 0; i < avg.length; i++) {
            if (avg[i] > avg[greatestAvg]) {
                greatestAvg = i;
            }

        }
        //displaying the highest average
        System.out.printf("The student with the highest average (%.1f%%) is %s", avg[greatestAvg], Chart[greatestAvg][0]);
        System.out.println();
    }

    /**
     * This method calculates and displays the names of students whose average
     * is below 50%
     *
     * @param avg
     */
    public static void averageLow(float[] avg) 
    {

        for (int i = 0; i < avg.length; i++) {
            //displaying the averages which are below 50
            if (avg[i] < 50) {
                System.out.printf("We are concerned with %s who has an average of %.1f%% ", Chart[i][0], avg[i]);
                System.out.println();
            }

        }
    }

    /**
     * Method to manually add data if User desires to
     *
     * @param Chart
     */
    private static void dataEntry(String[][] Chart) 
    {
        int nameIndex;
        int markIndex;

        System.out.println("Enter the student's name and their corresponding marks for 5 courses");

        // nested for loop to store name and grades
        for (nameIndex = 0; nameIndex < Chart.length; nameIndex++) {
            //initializing scanner
            Scanner ky = new Scanner(System.in);

            for (markIndex = 0; markIndex < Chart[nameIndex].length; markIndex++) {   //if loop to make sure the first column stores name
                if (markIndex == 0) {
                    System.out.println("Enter Student name");
                    Chart[nameIndex][markIndex] = ky.nextLine();
                    //to store grade if column is not 0
                } else {
                    System.out.println("Enter Grade");
                    Chart[nameIndex][markIndex] = ky.next();
                }

            }
            System.out.println("Student name and grade has been added");
        }
        System.out.println("Data has been entered");
    }

    //main method
    public static void main(String[] args) 
    {

        //initializing scanner for input  
        Scanner ky = new Scanner(System.in);

        //prompting user whether they want to manually enter student names and grades
        System.out.println("Do you want to enter student names and their corresponding marks?. Reply with 1 for yes or 0 for no");
        int answer = ky.nextInt();
        //checking if user said yes
        if (answer == 1) {

            //callng method to manually enter student details      
            dataEntry(Chart);

        } else {
            //initialise default grades and names
            initial(Chart);
        }

        //displays Chart which stores stuent details
        displayGrade();

        // calculates average0
        average();

    }

}
