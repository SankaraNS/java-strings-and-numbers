
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sanka
 */
public class PlayingWithNumbers 
{
    public static int largestNumber(int x, int y, int z)// method which takes in three numbers as arguments/ paramenters
    { int largest=0;       //logic
        if(x>=y && x>=z)
            largest = x;
         if(y>=z && y>=x)
            largest = y;
          if(z>=x && z>=y)
            largest = z;
       return largest;   //returning the largest number
    }
    public static void main(String[] args )
    { //initializing variables to store the three numbers
            int num1;
            int num2;
            int num3;

            // asking the user for the first number and storing it in num1
            System.out.println("Please enter the first number :");
            Scanner keyboard= new Scanner(System.in);
            num1= keyboard.nextInt();


             // asking the user for the second number and storing it in num2

            System.out.println("Please enter the second number :");
            num2= keyboard.nextInt();


             // asking the user for the third number and storing it in num3
            System.out.println("Please enter the third number :");
            num3= keyboard.nextInt();

            //the output where we call the method which returns the largest number
            System.out.println("the largest number is: "+largestNumber(num1,num2,num3));

      }
}
